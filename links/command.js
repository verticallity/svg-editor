// Download function copied from some Stack Overflow answer, but it works, so who cares.
function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}




var helpPages =
[
	[
		"HELP: Page 1 - Meta",
		"The 'help' command can be followed by a number for a specific help page",
		"The 'help' command can also be used with a word for help on specific commands or topics",
		"The console can be opened and closed with [TAB]",
		"Try 'help commands' for a list of commands"
	]
];
var helpLookup =
{
	commands :
		[
			"HELP: Command list",
			"Use 'help [COMMAND NAME]' for more info on a command",
			"help - displays help info",
			"resize - resize the SVG",
			"set - set a brush property",
			"path - draw a path",
			"rect - draw a rectangle",
			"circle - draw a circle",
			"edit - edit an existing element",
			"transform - use transformations to edit a shape",
			"redraw - draw a class again with current brush",
			"rm - remove a class",
			"io - input and output"
		],
	help : 
		[
			"HELP: The 'help' command",
			"The 'help' command takes 0 or 1 arguments",
			"With 0 arguments, the first help page is output",
			"One argument can either be used as a page number or a keyword"
		],
	resize :
		[
			"HELP: The 'resize' command",
			"The 'resize' command takes 2 arguments",
			"The arguments specify the desired new dimentions,",
			"width and height, in that order"
		],
	set :
		[
			"HELP: The 'set' command",
			"The 'set' command takes 2 arguments",
			"The first argument should be the property you want to set",
			"The second argument should be the new value for it",
			"Use 'help properties' for a list of property names"
		],
	properties :
		[
			"HELP: Properties list",
			"width - the stroke width",
			"cap - the stroke line cap",
			"join - the stroke line join",
			"stroke - the stroke color",
			"fill - the fill color"
		],
	path :
		[
			"HELP: The 'path' command",
			"The 'path' command takes a name/class followed by a valid SVG path"
		],
	rect :
		[
			"HELP: The 'rect' command",
			"The 'rect' command takes 5 or 7 arguments",
			"With 5 arguments, it takes a name/class, followed by x and y coordinates",
			"then lastly the width and height of the rectangle respectively",
			"With 7 arguments, x and y radii for rounded corners are added to the end"
		],
	circle :
		[
			"HELP: The 'circle' command",
			"The 'circle' command takes 4 arguments",
			"It takes a name/class, then x and y coordinates for the center, then a radius"
		],
	edit :
		[
			"HELP: The 'edit' command",
			"The 'edit' command takes 1 or 2 arguments",
			"The first argument specified a class name to edit",
			"If there are multiple elements in that class, a second argument may be used as an index"
		],
	transform :
		[
			"HELP: The 'transform' command",
			"The 'transform' command takes at least 2 arguments",
			"The first argument specifies a class name to transform",
			"The second is a set type (see 'help settypes')",
			"Then a transform type (see 'help tftypes')",
			"Then, depending on the transform type, any additional arguments"
		],
	settypes :
		[
			"HELP: Set type list",
			"set - overwrites the current attribute properties and sets new ones",
			"add - adds more properties on top of existing ones",
			"reset - resets the objects transform, no additional arguments needed"
		],
	tftypes :
		[
			"HELP: Transform type list",
			"matrix - apply a matrix where the first 4 arguments make up a 2x2 matrix between X and Y, and translate by the last 2",
			"translate - move the object, used with arguments TX, and optionally TY",
			"rotate - rotates the object by X degrees",
			"scale - scales the object (with one argument, uniformly, with 2 for X and Y scaling)",
			"skewX - skew such that vertical lines are rotated by the given angle",
			"skewY - skew such that horizontal lines are rotated by the given angle",	
		],
	redraw :
		[
			"HELP: The 'redraw' command",
			"The 'redraw' command takes an arbitrary amount of arguments",
			"Every argument should be the name of a class, which will be the ones it will affect"
		],
	rm :
		[
			"HELP: The 'rm' command",
			"The 'rm' command takes an arbitrary amount of arguments",
			"Every argument should be the name of a class, which will be the ones it will remove"
		],
	io :
		[
			"HELP: The 'io' command",
			"The 'io' command takes at least 1 argument",
			"Depending on the first argument, there may be more",
			"For a list of all I/O options, use 'help input' or 'help output'"
		],
	input :
		[
			"HELP: Input method list",
			"paste - paste from clipboard as SVG"
		],
	output :
		[
			"HELP: Output method list",
			"copy - copy to clipboard",
			"save - save to file, follow with [WORD] to save as [WORD].svg, otherwise new.svg"
		],
	
	me :
		[
			"No thanks, I have my own issues."
		]
};




// ------------
// | COMMANDS |
// ------------

// -------
// GENERAL
// -------
previewCommands.always = function(parts) {
	preview.textContent = "";
	
	var tempElems = document.getElementsByClassName('temp');
	for (var i = tempElems.length-1; i >= 0; i--) {
		tempElems[i].remove();
	}
	
	return "";
}
previewCommands.regular = function(parts) {
	return "";
}

// -----
// FRAME
// -----
commands.resize = function(arguments) {
	if (arguments.length >= 2) {
		if (arguments.length > 2) {
			writeToConsoleSpecial("note", "All but first 2 arguments ignored");
		}
		
		if (isNaN(arguments[0]) || isNaN(arguments[1])) {
			return "Dimentions must be evaluatable as numbers";
		}
		
		if (+arguments[0] <= 0 || +arguments[1] <= 0) {
			return "Dimentions must be greater than 0";
		}
		
		
		mainSVG.setAttribute("width", arguments[0]);
		mainSVG.setAttribute("height", arguments[1]);
		bgPlane.setAttribute("width", arguments[0]);
		bgPlane.setAttribute("height", arguments[1]);
		svgInfo.width = arguments[0];
		svgInfo.height = arguments[1];
		
		return 0;
	}
	
	return "'resize' takes 2 arguments";
}
previewCommands.resize = function(arguments) {
	if (arguments.length >= 2) {
		if ((!isNaN(arguments[0])) && (!isNaN(arguments[0]))) {
			if ((+arguments[0]) > 0 && (+arguments[1]) > 0) {
				preview.setAttribute("width", arguments[0]);
				preview.setAttribute("height", arguments[1]);
				preview.innerHTML = "<rect width=\"" + arguments[0] + "\" height=\"" + arguments[1] + "\" stroke=\"black\" stroke-width=\"2\" fill=\"none\" ></rect>";
				return "";
			}
		}
	}
	
	preview.textContent = "";
	return "";
}


// ---
// SET
// ---
commands.set = function(arguments) {
	if (arguments.length === 2) {
		switch (arguments[0]) {
		case "width":
			if (!isNaN(arguments[1])) {
				if (+arguments[1] >= 0) {
					brushInfo.width = +arguments[1];
					return 0;
				}
			}
			return "Invalid value for 'width'";
		case "cap":
			if (arguments[1].match(/none|round|square/)) {
				brushInfo.cap = arguments[1];
				return 0;
			}
			return "Invalid value for 'cap'";
		case "join":
			if (arguments[1].match(/none|round|square/)) {
				brushInfo.join = arguments[1];
				return 0;
			}
			return "Invalid value for 'join'";
		case "stroke":
			if (arguments[1].match(colorStructure)) {
				brushInfo.stroke = arguments[1];
				return 0;
			}
			return "Invalid value for 'stroke'";
		case "fill":
			if (arguments[1].match(colorStructure)) {
				brushInfo.fill = arguments[1];
				return 0;
			}
			return "Invalid value for 'fill'";
		default:
			return "Not a valid variable keyword";
		}
		
		return 0;
	}
	
	return "'set' takes 2 arguments";
}
const colorStructure = /^(none|#[0-9a-f]{3}|#[0-9a-f]{6}|rgba\(\s*\d+\s*,\s*\d+\s*,\s*\d+\s*,\s*-?\d*(\d|\.\d+)\s*\))$/i;

previewCommands.set = function(arguments) {
	if (arguments.length < 1) {
		return "PROPERTY VALUE";
	}
	
		switch (arguments[0]) {
		case "width":
			return "NUMBER";
		case "cap":
		case "join":
			return "NONE|ROUND|SQUARE";
		case "stroke":
		case "fill":
			return "NONE|COLOR";
		default:
			return "NOT A KEYWORD";
	}
	
	return "";
}

commands.group = function(arguments) {
	if (arguments.length < 2) {
		return "'group' takes at least 2 arguments";
	}
	
	var toAdd = "<g class=\"insvg" + escapeHtml(arguments.shift()) + "\">"
	
	for (var i = 0; i < arguments.length; i++) {
		var currentElements = document.getElementsByClassName("insvg"+escapeHtml(arguments[i]));
		
		if (currentElements.length === 0) {
			writeToConsoleSpecial("note", "No elements in class "+arguments[i]);
		} else {
			for (var j = currentElements.length-1; j >= 0; j--) {
				toAdd += currentElements[j].outerHTML;
				currentElements[j].outerHTML = "";
			}
		}
	}
	
	mainSVG.innerHTML += toAdd + "</g>";
	
	return 0;
}
previewCommands.group = function(arguments) {
	if (arguments.length < 2) {
		return "GROUPNAME( CLASS/NAME)+";
	}
	
	var toAdd = "<g class=\"insvg" + escapeHtml(arguments.shift()) + "\">"
	
	for (var i = 0; i < arguments.length; i++) {
		var currentElements = document.getElementsByClassName("insvg"+escapeHtml(arguments[i]));
		
		if (currentElements.length > 0) {
			for (var j = currentElements.length-1; j >= 0; j--) {
				if (currentElements[j].getAttribute("stroke") != "none") {
					currentElements[j].innerHTML += "<animate attributeType=\"XML\" attributeName=\"stroke\" begin=\"0s\" dur=\"0.5s\" repeatCount=\"indefinite\" values=\"#eaa;#f33;#eaa;\" calcMode=\"spline\" keySplines=\"0.5 0 0.5 1; 0.5 0 0.5 1;\"  class=\"temp\" ></animate>";
				}
				if (currentElements[j].getAttribute("fill") != "none") {
					currentElements[j].innerHTML += "<animate attributeType=\"XML\" attributeName=\"fill\" begin=\"0s\" dur=\"0.5s\" repeatCount=\"indefinite\" values=\"#eaa;#f33;#eaa;\" calcMode=\"spline\" keySplines=\"0.5 0 0.5 1; 0.5 0 0.5 1;\"  class=\"temp\" ></animate>";
				}
			}
		}
	}
	
	return "";
}

// ----
// DRAW
// ----
commands.path = function(arguments) {
	if (arguments.length < 2) {
		return "'path' takes at least 2 arguments";
	}
	
	var pathClass = arguments.shift();
	var pathStr = arguments.join(" ")
	
	var replacedPathStr = pathStr.replace(/([MLTHVQSCAZ])([^MLTHVQSCAZ]*)\s*\/\s*([MLTHVQSCAZ])/ig, "$1$2 m0,0 $3").replace(/([MLTHVQSCAZ])([^MLTHVQSCAZ]*)\s*\/\s*(\d)/ig, "$1$2 m0,0 $1$3");
	
	// Had some trouble with getting it to replace properly, this is just the best way for now
	while (pathStr != replacedPathStr) {
		pathStr = replacedPathStr;
		replacedPathStr = pathStr.replace(/([MLTHVQSCAZ])([^MLTHVQSCAZ]*)\s*\/\s*([MLTHVQSCAZ])/ig, "$1$2 m0,0 $3").replace(/([MLTHVQSCAZ])([^MLTHVQSCAZ]*)\s*\/\s*(\d)/ig, "$1$2 m0,0 $1$3");
	}
	
	
	var pathMatch = pathStr.match(pathStructure);
	
	if (!(pathMatch)) {
		return "Invalid path structure";
	}
	
	if (pathMatch[0].length < pathStr.length) {
		writeToConsoleSpecial("note", "Path elements ignored following error");
	}
	
	mainSVG.innerHTML += "<path class=\"insvg" + pathClass + "\" d=\"" + escapeHtml(pathStr.substr(0, pathMatch[0].length)) + "\" stroke=\"" + brushInfo.stroke + "\" stroke-width=\"" + brushInfo.width + "\" stroke-linecap=\"" + brushInfo.cap + "\" stroke-linejoin=\"" + brushInfo.join + "\" fill=\"" + brushInfo.fill + "\" ></path>";
	
	return 0;
};
previewCommands.path = function(arguments) {
	if (arguments.length < 2) {
		return "NAME/CLASS( PATH)+";
	}
	
	arguments.shift();
	
	
	var pathStr = arguments.join(" ")
	
	var replacedPathStr = pathStr.replace(/([MLTHVQSCAZ])([^MLTHVQSCAZ]*)\s*\/\s*([MLTHVQSCAZ])/ig, "$1$2 m0,0 $3").replace(/([MLTHVQSCAZ])([^MLTHVQSCAZ]*)\s*\/\s*(\d)/ig, "$1$2 m0,0 $1$3");
	
	// Had some trouble with getting it to replace properly, this is just the best way for now
	while (pathStr != replacedPathStr) {
		pathStr = replacedPathStr;
		replacedPathStr = pathStr.replace(/([MLTHVQSCAZ])([^MLTHVQSCAZ]*)\s*\/\s*([MLTHVQSCAZ])/ig, "$1$2 m0,0 $3").replace(/([MLTHVQSCAZ])([^MLTHVQSCAZ]*)\s*\/\s*(-|\.|\d)/ig, "$1$2 m0,0 $1$3");
	}
	
	
	// Test to prevent clogging of the console with errors about path structure
	var pathMatch = pathStr.match(pathStructure);
	if (pathMatch) {
		preview.innerHTML = "<path d=\"" + escapeHtml(pathStr.substr(0, pathMatch[0].length /* Only show up to error, makes it smoother for typing */)) + "\" stroke=\"" + brushInfo.stroke + "\" stroke-width=\"" + brushInfo.width + "\" stroke-linecap=\"" + brushInfo.cap + "\" stroke-linejoin=\"" + brushInfo.join + "\" fill=\"" + brushInfo.fill + "\"></path>";
	}
	
	return "";
};

// Kept so that I don't lose my mind when I want to edit pathStructure
const pathArgumentStructures =
{
	MLT : /[MLT]\s*-?\d*(\d|\.\d+)\s*[,\s]\s*-?\d*(\d|\.\d+)/ig,
	MRepeat : /((\s*[,\s]\s*-?\d*(\d|\.\d+)){2})*/ig,
	HV : /[HV]\s*-?\d*(\d|\.\d+)/ig,
	HRepeat : /(\s*[,\s]\s*-?\d*(\d|\.\d+))*/ig,
	QS : /[QS]\s*(-?\d*(\d|\.\d+)\s*[,\s]\s*){3}-?\d*(\d|\.\d+)/ig,
	QRepeat : /((\s*[,\s]\s*-?\d*(\d|\.\d+)){4})*/ig,
	C : /C\s*(-?\d*(\d|\.\d+)\s*[,\s]\s*){5}-?\d*(\d|\.\d+)/ig,
	CRepeat : /((\s*[,\s]\s*-?\d*(\d|\.\d+)){6})*/ig,
	A : /A\s*(-?\d*(\d|\.\d+)\s*[,\s]\s*){6}-?\d*(\d|\.\d+)/ig,
	ARepeat : /((\s*[,\s]\s*-?\d*(\d|\.\d+)){7})*/ig,
	Z : /Z/ig
}
// Just a one regex thing for the whole path, made from the ones above + some overall structure
// Please kill me.
const pathStructure = /^\s*M\s*-?\d*(\d|\.\d+)\s*[,\s]\s*-?\d*(\d|\.\d+)((\s*[,\s]\s*-?\d*(\d|\.\d+)){2})*\s*(([HV]\s*-?\d*(\d|\.\d+)(\s*[,\s]\s*-?\d*(\d|\.\d+))*|[MLT]\s*-?\d*(\d|\.\d+)\s*[,\s]\s*-?\d*(\d|\.\d+)((\s*[,\s]\s*-?\d*(\d|\.\d+)){2})*|[QS]\s*(-?\d*(\d|\.\d+)\s*[,\s]\s*){3}-?\d*(\d|\.\d+)((\s*[,\s]\s*-?\d*(\d|\.\d+)){4})*|C\s*(-?\d*(\d|\.\d+)\s*[,\s]\s*){5}-?\d*(\d|\.\d+)((\s*[,\s]\s*-?\d*(\d|\.\d+)){6})*|A\s*(-?\d*(\d|\.\d+)\s*[,\s]\s*){6}-?\d*(\d|\.\d+)((\s*[,\s]\s*-?\d*(\d|\.\d+)){7})*|Z)\s*)+/i;
// Spaced out because JS doesn't allow free-spacing mode
/*
/
^
\s*                                          # Optional starting whitespace
M\s*-?\d*(\d|\.\d+)\s*[,\s]\s*-?\d*(\d|\.\d+)    # Must start with at least 1 move command to determine starting location
((\s*[,\s]\s*-?\d*(\d|\.\d+)){2})*             # Match any repeated move commands (allowed without another 'M')
\s*

(
	(
		 [HV]\s*-?\d*(\d|\.\d+)                                # Match any 1 number commands (H or V)
		   (\s*[,\s]\s*-?\d*(\d|\.\d+))*                       # repeats
		|[MLT]\s*-?\d*(\d|\.\d+)\s*[,\s]\s*-?\d*(\d|\.\d+)       # Match any 2 number commands (M, L, or T)
		   ((\s*[,\s]\s*-?\d*(\d|\.\d+)){2})*                  # repeats
		|[QS]\s*(-?\d*(\d|\.\d+)\s*[,\s]\s*){3}-?\d*(\d|\.\d+)   # Match any 4 number commands (Q or S)
		   ((\s*[,\s]\s*-?\d*(\d|\.\d+)){4})*                  # repeats
		|C\s*(-?\d*(\d|\.\d+)\s*[,\s]\s*){5}-?\d*(\d|\.\d+)      # Match any 6 number commands (C)
		   ((\s*[,\s]\s*-?\d*(\d|\.\d+)){6})*                  # repeats
		|A\s*(-?\d*(\d|\.\d+)\s*[,\s]\s*){6}-?\d*(\d|\.\d+)      # Match any 7 number commands (A)
		   ((\s*[,\s]\s*-?\d*(\d|\.\d+)){7})*                  # repeats
		|Z                                                   # Match any 0 number commands (Z) (no repeats, there's nothing against it, it's just impossible to type)
	)
	\s*                                      # Trailing whitespace
)+                                           # Match multiple commands

/i
*/

commands.rect = function(arguments) {
	if (arguments.length < 5) {
		return "'rect' takes 5 or 7 arguments";
	}
	
	if (arguments.length > 7 || arguments.length === 6) {
		writeToConsoleSpecial("note", "Some arguments ignored");
	}
	
	if (isNaN(arguments[1]) || isNaN(arguments[2]) || isNaN(arguments[3]) || isNaN(arguments[4])) {
		return "'rect' takes 4 or 6 numbers";
	}
	
	if (+arguments[3] < 0 || +arguments[4] < 0) {
		return "dimentions must be positive numbers";
	}
	
	if (arguments.length < 7) {
		mainSVG.innerHTML += "<rect class=\"insvg" + arguments[0] + "\" x=\"" + arguments[1] + "\" y=\"" + arguments[2] + "\" width=\"" + arguments[3] + "\" height=\"" + arguments[4] + "\" stroke=\"" + brushInfo.stroke + "\" stroke-width=\"" + brushInfo.width + "\" stroke-linecap=\"" + brushInfo.cap + "\" stroke-linejoin=\"" + brushInfo.join + "\" fill=\"" + brushInfo.fill + "\" ></rect>";
	} else {
		if (!isNaN(arguments[5]) && !isNaN(arguments[6])) {
			if (+arguments[5] >= 0 && +arguments[6] >= 0) {
				mainSVG.innerHTML += "<rect class=\"insvg" + arguments[0] + "\" x=\"" + arguments[1] + "\" y=\"" + arguments[2] + "\" width=\"" + arguments[3] + "\" height=\"" + arguments[4] + "\" rx=\"" + arguments[5] + "\" ry=\"" + arguments[6] + "\" stroke=\"" + brushInfo.stroke + "\" stroke-width=\"" + brushInfo.width + "\" stroke-linecap=\"" + brushInfo.cap + "\" stroke-linejoin=\"" + brushInfo.join + "\" fill=\"" + brushInfo.fill + "\" ></rect>";
				
				return 0;
			}
		}
		
		writeToConsoleSpecial("note", "Radii ignored, invalid property values");
		mainSVG.innerHTML += "<rect class=\"insvg" + arguments[0] + "\" x=\"" + arguments[1] + "\" y=\"" + arguments[2] + "\" width=\"" + arguments[3] + "\" height=\"" + arguments[4] + "\" stroke=\"" + brushInfo.stroke + "\" stroke-width=\"" + brushInfo.width + "\" stroke-linecap=\"" + brushInfo.cap + "\" stroke-linejoin=\"" + brushInfo.join + "\" fill=\"" + brushInfo.fill + "\" ></rect>";
		return 0;
	}
	
	return 0;
};
previewCommands.rect = function(arguments) {
	if (arguments.length < 5) {
		return "NAME/CLASS X Y WIDTH HEIGHT";
	}
	
	if (isNaN(arguments[1]) || isNaN(arguments[2]) || isNaN(arguments[3]) || isNaN(arguments[4])) {
		return "'x', 'y', 'width', and 'height' must be numbers";
	}
	
	if (+arguments[3] < 0 || +arguments[4] < 0) {
		return "dimentions must be positive numbers";
	}
	
	if (arguments.length < 7) {
		preview.innerHTML = "<rect class=\"insvg" + arguments[0] + "\" x=\"" + arguments[1] + "\" y=\"" + arguments[2] + "\" width=\"" + arguments[3] + "\" height=\"" + arguments[4] + "\" stroke=\"" + brushInfo.stroke + "\" stroke-width=\"" + brushInfo.width + "\" stroke-linecap=\"" + brushInfo.cap + "\" stroke-linejoin=\"" + brushInfo.join + "\" fill=\"" + brushInfo.fill + "\" ></rect>";
	} else {
		if (!isNaN(arguments[5]) && !isNaN(arguments[6])) {
			if (+arguments[5] >= 0 && +arguments[6] >= 0) {
				preview.innerHTML = "<rect class=\"insvg" + arguments[0] + "\" x=\"" + arguments[1] + "\" y=\"" + arguments[2] + "\" width=\"" + arguments[3] + "\" height=\"" + arguments[4] + "\" rx=\"" + arguments[5] + "\" ry=\"" + arguments[6] + "\" stroke=\"" + brushInfo.stroke + "\" stroke-width=\"" + brushInfo.width + "\" stroke-linecap=\"" + brushInfo.cap + "\" stroke-linejoin=\"" + brushInfo.join + "\" fill=\"" + brushInfo.fill + "\" ></rect>";
				
				return "";
			}
		}
		
		preview.innerHTML = "<rect class=\"insvg" + arguments[0] + "\" x=\"" + arguments[1] + "\" y=\"" + arguments[2] + "\" width=\"" + arguments[3] + "\" height=\"" + arguments[4] + "\" stroke=\"" + brushInfo.stroke + "\" stroke-width=\"" + brushInfo.width + "\" stroke-linecap=\"" + brushInfo.cap + "\" stroke-linejoin=\"" + brushInfo.join + "\" fill=\"" + brushInfo.fill + "\" ></rect>";
		return "";
	}
	
	return "";
};


commands.circle = function(arguments) {
	if (arguments.length < 4) {
		return "'circle' takes 4 arguments"
	}
	
	if (arguments.length > 4) {
		writeToConsoleSpecial("note", "Excess arguments ignored");
	}
	
	if (isNaN(arguments[1]) || isNaN(arguments[2]) || isNaN(arguments[3])) {
		return "'cx', 'cy', and 'r' must all be numbers"
	}
	
	if (+arguments[3] < 0) {
		return "radius must be a positive number";
	}
	
	mainSVG.innerHTML += "<circle class=\"insvg" + arguments[0] + "\" cx=\"" + arguments[1] + "\" cy=\"" + arguments[2] + "\" r=\"" + arguments[3] + "\" stroke=\"" + brushInfo.stroke + "\" stroke-width=\"" + brushInfo.width + "\" stroke-linecap=\"" + brushInfo.cap + "\" stroke-linejoin=\"" + brushInfo.join + "\" fill=\"" + brushInfo.fill + "\"></circle>";
	return 0;
}
previewCommands.circle = function(arguments) {
	if (arguments.length < 4) {
		return "NAME/CLASS CX CY R";
	}
	
	if (isNaN(arguments[1]) || isNaN(arguments[2]) || isNaN(arguments[3])) {
		return "'cx', 'cy', and 'r' must all be numbers"
	}
	
	if (+arguments[3] < 0) {
		return "radius must be a positive number";
	}
	
	preview.innerHTML += "<circle class=\"insvg" + arguments[0] + "\" cx=\"" + arguments[1] + "\" cy=\"" + arguments[2] + "\" r=\"" + arguments[3] + "\" stroke=\"" + brushInfo.stroke + "\" stroke-width=\"" + brushInfo.width + "\" stroke-linecap=\"" + brushInfo.cap + "\" stroke-linejoin=\"" + brushInfo.join + "\" fill=\"" + brushInfo.fill + "\"></circle>";
	return "";
}



// ----
// EDIT
// ----

commands.edit = function(arguments) {
	if (arguments.length === 0) {
		return "NAME/CLASS( INDEX)";
	}
	
	if (arguments.length > 1) {
		if (arguments.length > 2) {
			writeToConsoleSpecial("note", "Excess arguments ignored");
		}
		
		if (isNaN(arguments[1])) {
			return "Index is not a number";
		}
		
		var editElems = document.getElementsByClassName('insvg'+arguments[0]);
		
		if (arguments[1] >= editElems.length || arguments[1] < 0) {
			return "Index outside of range";
		}
		
		var editElem = editElems[arguments[1]];
	} else {
		var editElems = document.getElementsByClassName('insvg'+arguments[0]);
		
		if (editElems.length === 0) {
			return "No elements in class";
		}
		
		var editElem = editElems[0];
	}
	
	switch (editElem.tagName) {
	case "path":
		currentInput = "path "+arguments[0]+" "+editElem.getAttribute('d');
		cursorPosition = currentInput.length;
		editElem.remove();
		updateTextPrompt();
		
		break;
	case "rect":
		currentInput = "rect "+arguments[0]+" "+editElem.getAttribute('x')+" "+editElem.getAttribute('y')+" "+editElem.getAttribute('width')+" "+editElem.getAttribute('height');
		cursorPosition = currentInput.length;
		editElem.remove();
		updateTextPrompt();
		
		break;
	case "circle":
		currentInput = "circle "+arguments[0]+" "+editElem.getAttribute('cx')+" "+editElem.getAttribute('cy')+" "+editElem.getAttribute('r');
		cursorPosition = currentInput.length;
		editElem.remove();
		updateTextPrompt();
		
		break;
	default:
		return "Uneditable type";
		break;
	}
	
	return 0;
}
previewCommands.edit = function(arguments) {
	if (arguments.length === 0) {
		return "NAME/CLASS( INDEX)";
	}
	
	if (arguments.length > 1) {
		if (isNaN(arguments[1])) {
			return "Index should be a number";
		}
		
		var editElems = document.getElementsByClassName('insvg'+arguments[0]);
		
		if (arguments[1] >= editElems.length || arguments[1] < 0) {
			return "Index outside of range";
		}
		
		var editElem = editElems[arguments[1]];
	} else {
		var editElems = document.getElementsByClassName('insvg'+arguments[0]);
		
		if (editElems.length === 0) {
			return "No elements in class";
		}
		
		var editElem = editElems[0];
	}
	
	
	if (editElem.getAttribute("stroke") != "none") {
		editElem.innerHTML += "<animate attributeType=\"XML\" attributeName=\"stroke\" begin=\"0s\" dur=\"0.5s\" repeatCount=\"indefinite\" values=\"#eaa;#f33;#eaa;\" calcMode=\"spline\" keySplines=\"0.5 0 0.5 1; 0.5 0 0.5 1;\"  class=\"temp\" ></animate>";
	}
	if (editElem.getAttribute("fill") != "none") {
		editElem.innerHTML += "<animate attributeType=\"XML\" attributeName=\"fill\" begin=\"0s\" dur=\"0.5s\" repeatCount=\"indefinite\" values=\"#eaa;#f33;#eaa;\" calcMode=\"spline\" keySplines=\"0.5 0 0.5 1; 0.5 0 0.5 1;\"  class=\"temp\" ></animate>";
	}
	
	
	return "";
}


commands.transform = function(arguments) {
	if (arguments.length < 2) {
		return "Transform takes at least 3 arguments";
	} else if (arguments.length < 3) {
		if (arguments[1] != "reset") {
			return "Transform takes at least 3 arguments";
		}
	}
	
	var currentElements = document.getElementsByClassName('insvg'+arguments.shift());
	
	var setType = arguments.shift();
	
	if (setType === "reset") {
		if (arguments.length > 0) {
			writeToConsoleSpecial("note", "Excess arguments ignored");
		}
		
		for (var i = 0; i < currentElements.length; i++) {
			currentElements[i].setAttribute("transform", "");
		}
		return 0;
	}
	
	if (setType != "set" && setType != "add") {
		return "Invalid set type";
	}
	
	var transType = arguments.shift();
	
	if (transType === "matrix") {
		if (arguments.length < 6) {
			return "Matrix takes 6 additional arguments";
		}
		
		if (arguments.length > 6) {
			writeToConsoleSpecial("note", "Excess arguments ignored");
			arguments = arguments.splice(0,6);
		}
		
		for (var i = 0; i < arguments.length; i++) {
			if (isNaN(arguments[i])) {
				return "All transform values must be numbers";
			}
		}
		
		// Get it‽
		var transFormat = " matrix("+arguments.join(",")+")";
	} else if (transType === "translate") {
		if (arguments.length === 0) {
			return "Translate takes 1 or 2 additional arguments";
		}
		
		if (arguments.length > 2) {
			writeToConsoleSpecial("note", "Excess arguments ignored");
			arguments = arguments.splice(0,2);
		}
		
		for (var i = 0; i < arguments.length; i++) {
			if (isNaN(arguments[i])) {
				return "All transform values must be numbers";
			}
		}
		
		var transFormat = " translate("+arguments.join(",")+")";
	} else if (transType === "scale") {
		if (arguments.length === 0) {
			return "Scale takes 1 or 2 additional arguments";
		}
		
		if (arguments.length > 2) {
			writeToConsoleSpecial("note", "Excess arguments ignored");
			arguments = arguments.splice(0,2);
		}
		
		for (var i = 0; i < arguments.length; i++) {
			if (isNaN(arguments[i])) {
				return "All transform values must be numbers";
			}
		}
		
		var transFormat = " scale("+arguments.join(",")+")";
	} else if (transType === "rotate") {
		if (arguments.length === 0) {
			return "Rotate takes 1 or 3 additional arguments";
		}
		
		if (arguments.length > 3) {
			writeToConsoleSpecial("note", "Excess arguments ignored");
			arguments = arguments.splice(0,3);
		} else if (arguments.length === 2) {
			writeToConsoleSpecial("note", "Excess arguments ignored");
			arguments = [arguments[0]];
		}
		
		for (var i = 0; i < arguments.length; i++) {
			if (isNaN(arguments[i])) {
				return "All transform values must be numbers";
			}
		}
		
		var transFormat = " rotate("+arguments.join(",")+")";
	} else if (transType === "skewX") {
		if (arguments.length < 1) {
			return "SkewX takes 1 additional arguments";
		}
		
		if (arguments.length > 1) {
			writeToConsoleSpecial("note", "Excess arguments ignored");
			arguments = [arguments[0]];
		}
		
		for (var i = 0; i < arguments.length; i++) {
			if (isNaN(arguments[i])) {
				return "All transform values must be numbers";
			}
		}
		
		var transFormat = " skewX("+arguments.join(",")+")";
	} else if (transType === "skewY") {
		if (arguments.length < 1) {
			return "SkewY takes 1 additional arguments";
		}
		
		if (arguments.length > 1) {
			writeToConsoleSpecial("note", "Excess arguments ignored");
			arguments = [arguments[0]];
		}
		
		for (var i = 0; i < arguments.length; i++) {
			if (isNaN(arguments[i])) {
				return "All transform values must be numbers";
			}
		}
		
		var transFormat = " skewY("+arguments.join(",")+")";
	} else {
		return "Invalid transform type";
	}
	
	for (var i = 0; i < currentElements.length; i++) {
		var currTransform = currentElements[i].getAttribute("transform");
		currTransform = ((currTransform === null) ? ("") : (currTransform));
		
		if (setType === "add" || !currTransform.match(transType)) {
			currentElements[i].setAttribute("transform", currTransform + transFormat);
		} else {
			currentElements[i].setAttribute("transform", (currTransform.replace(RegExp(transType+".*?\\)", "g"), "") + transFormat).replace(/^\s+|\s+$/g, ""));
		}
	}
	
	return 0;
}
previewCommands.transform = function(arguments) {
	if (arguments.length === 1) {
		var currentElements = document.getElementsByClassName('insvg'+arguments.shift());
		
		for (var i = 0; i < currentElements.length; i++) {
			if (currentElements[i].getAttribute("stroke") != "none") {
				currentElements[i].innerHTML += "<animate attributeType=\"XML\" attributeName=\"stroke\" begin=\"0s\" dur=\"0.5s\" repeatCount=\"indefinite\" values=\"#eaa;#f33;#eaa;\" calcMode=\"spline\" keySplines=\"0.5 0 0.5 1; 0.5 0 0.5 1;\"  class=\"temp\" ></animate>";
			}
			if (currentElements[i].getAttribute("fill") != "none") {
				currentElements[i].innerHTML += "<animate attributeType=\"XML\" attributeName=\"fill\" begin=\"0s\" dur=\"0.5s\" repeatCount=\"indefinite\" values=\"#eaa;#f33;#eaa;\" calcMode=\"spline\" keySplines=\"0.5 0 0.5 1; 0.5 0 0.5 1;\"  class=\"temp\" ></animate>";
			}
		}
	}
	
	if (arguments.length < 2) {
		return "NAME/CLASS SETMODE TYPE( VALUE)+";
	} else if (arguments.length < 3) {
		if (arguments[1] != "reset") {
			return "NAME/CLASS SETMODE TYPE( VALUE)+";
		}
	}
	
	var currentElements = document.getElementsByClassName('insvg'+arguments.shift());
	
	var setType = arguments.shift();
	
	if (setType === "reset") {
		for (var i = 0; i < currentElements.length; i++) {
			preview.innerHTML += currentElements[i].outerHTML.replace(/ transform=\".*?\"/, "").replace(/ class=\".*?\"/, "");
		}
		return "";
	}
	
	if (setType != "set" && setType != "add") {
		return "Invalid set type";
	}
	
	var transType = arguments.shift();
	
	if (transType === "matrix") {
		if (arguments.length < 6) {
			return "A B C D TX TY";
		}
		
		if (arguments.length > 6) {
			arguments = arguments.splice(0,6);
		}
		
		for (var i = 0; i < arguments.length; i++) {
			if (isNaN(arguments[i])) {
				return "All transform values must be numbers";
			}
		}
		
		// Get it‽
		var transFormat = " matrix("+arguments.join(",")+")";
	} else if (transType === "translate") {
		if (arguments.length === 0) {
			return "TX( TY)";
		}
		
		if (arguments.length > 2) {
			arguments = arguments.splice(0,2);
		}
		
		for (var i = 0; i < arguments.length; i++) {
			if (isNaN(arguments[i])) {
				return "All transform values must be numbers";
			}
		}
		
		var transFormat = " translate("+arguments.join(",")+")";
	} else if (transType === "scale") {
		if (arguments.length === 0) {
			return "SX( SY)";
		}
		
		if (arguments.length > 2) {
			arguments = arguments.splice(0,2);
		}
		
		for (var i = 0; i < arguments.length; i++) {
			if (isNaN(arguments[i])) {
				return "All transform values must be numbers";
			}
		}
		
		var transFormat = " scale("+arguments.join(",")+")";
	} else if (transType === "rotate") {
		if (arguments.length === 0) {
			return "ANGLE( CX CY)";
		}
		
		if (arguments.length > 3) {
			arguments = arguments.splice(0,3);
		} else if (arguments.length === 2) {
			arguments = [arguments[0]];
		}
		
		for (var i = 0; i < arguments.length; i++) {
			if (isNaN(arguments[i])) {
				return "All transform values must be numbers";
			}
		}
		
		var transFormat = " rotate("+arguments.join(",")+")";
	} else if (transType === "skewX") {
		if (arguments.length < 1) {
			return "ANGLE";
		}
		
		if (arguments.length > 1) {
			arguments = [arguments[0]];
		}
		
		for (var i = 0; i < arguments.length; i++) {
			if (isNaN(arguments[i])) {
				return "All transform values must be numbers";
			}
		}
		
		var transFormat = " skewX("+arguments.join(",")+")";
	} else if (transType === "skewY") {
		if (arguments.length < 1) {
			return "ANGLE";
		}
		
		if (arguments.length > 1) {
			arguments = [arguments[0]];
		}
		
		for (var i = 0; i < arguments.length; i++) {
			if (isNaN(arguments[i])) {
				return "All transform values must be numbers";
			}
		}
		
		var transFormat = " skewY("+arguments.join(",")+")";
	} else {
		return "Invalid transform type";
	}
	
	for (var i = 0; i < currentElements.length; i++) {
		var currTransform = currentElements[i].getAttribute("transform");
		
		if (currTransform === null) {
			currentElements[i].setAttribute("transform", " ");
			currTransform = "";
		}
		
		if (setType === "add" || !currTransform.match(transType)) {
			preview.innerHTML += currentElements[i].outerHTML.replace(/ transform=\".*?\"/, " transform=\"" + currTransform + transFormat + "\"").replace(/ class=\".*?\"/, "");
		} else {
			preview.innerHTML += currentElements[i].outerHTML.replace(RegExp(transType+".*?\\)", "g"), "").replace(/(transform=".*?)"/, "$1"+transFormat+"\"").replace(/ class=\".*?\"/, "");
		}
	}
	
	return "";
}



commands.redraw = function(arguments) {
	if (arguments.length === 0) {
		writeToConsoleSpecial("note", "Nothing to redraw");
	} else {
		for (var i = 0; i < arguments.length; i++) {
			var currentElements = document.getElementsByClassName('insvg'+arguments[i]);
			
			if (currentElements.length === 0) {
				writeToConsoleSpecial("note", "No members of "+arguments[i]);
			} else {
				for (var j = 0; j < currentElements.length; j++) {
					currentElements[j].setAttribute("stroke-width",    brushInfo.width);
					currentElements[j].setAttribute("stroke-linecap",  brushInfo.cap);
					currentElements[j].setAttribute("stroke-linejoin", brushInfo.join);
					currentElements[j].setAttribute("stroke",          brushInfo.stroke);
					currentElements[j].setAttribute("fill",            brushInfo.fill);
				}
			}
		}
	}
	
	return 0;
};
previewCommands.redraw = function(arguments) {
	if (arguments.length === 0) {
		return "NAME/CLASS( NAME/CLASS)*";
	}
	for (var i = 0; i < arguments.length; i++) {
		var currentElements = document.getElementsByClassName('insvg'+arguments[i]);
		
		for (var j = 0; j < currentElements.length; j++) {
			if (currentElements[j].getAttribute("stroke") != "none") {
				currentElements[j].innerHTML += "<animate attributeType=\"XML\" attributeName=\"stroke\" begin=\"0s\" dur=\"0.5s\" repeatCount=\"indefinite\" values=\"#eaa;#f33;#eaa;\" calcMode=\"spline\" keySplines=\"0.5 0 0.5 1; 0.5 0 0.5 1;\"  class=\"temp\" ></animate>";
			}
			if (currentElements[j].getAttribute("fill") != "none") {
				currentElements[j].innerHTML += "<animate attributeType=\"XML\" attributeName=\"fill\" begin=\"0s\" dur=\"0.5s\" repeatCount=\"indefinite\" values=\"#eaa;#f33;#eaa;\" calcMode=\"spline\" keySplines=\"0.5 0 0.5 1; 0.5 0 0.5 1;\"  class=\"temp\" ></animate>";
			}
		}
	}
	
	return "";
}

commands.rm = function(arguments) {
	if (arguments.length === 0) {
		writeToConsoleSpecial("note", "Nothing to remove");
	} else {
		for (var i = 0; i < arguments.length; i++) {
			var currentElements = document.getElementsByClassName('insvg'+arguments[i]);
			
			if (currentElements.length === 0) {
				writeToConsoleSpecial("note", "No members of "+arguments[i]);
			} else {
				for (var j = currentElements.length-1; j >= 0; j--) {
					currentElements[j].remove();
				}
			}
		}
	}
	
	return 0;
};
previewCommands.rm = function(arguments) {
	if (arguments.length === 0) {
		return "NAME/CLASS( NAME/CLASS)*";
	}
	for (var i = 0; i < arguments.length; i++) {
		var currentElements = document.getElementsByClassName('insvg'+arguments[i]);
		
		for (var j = 0; j < currentElements.length; j++) {
			if (currentElements[j].getAttribute("stroke") != "none") {
				currentElements[j].innerHTML += "<animate attributeType=\"XML\" attributeName=\"stroke\" begin=\"0s\" dur=\"0.5s\" repeatCount=\"indefinite\" values=\"#eaa;#f33;#eaa;\" calcMode=\"spline\" keySplines=\"0.5 0 0.5 1; 0.5 0 0.5 1;\"  class=\"temp\" ></animate>";
			}
			if (currentElements[j].getAttribute("fill") != "none") {
				currentElements[j].innerHTML += "<animate attributeType=\"XML\" attributeName=\"fill\" begin=\"0s\" dur=\"0.5s\" repeatCount=\"indefinite\" values=\"#eaa;#f33;#eaa;\" calcMode=\"spline\" keySplines=\"0.5 0 0.5 1; 0.5 0 0.5 1;\"  class=\"temp\" ></animate>";
			}
		}
	}
	
	return "";
}




// --
// IO
// --

commands.io = function(arguments) {
	if (arguments.length >= 1) {
		var outAsText = svgCont.innerHTML.match(/^.*?<\/svg>/)[0].replace(/(<svg.*) id=".*?"(.*>)/, "$1$2");
		
		switch (arguments[0]) {
		case "copy":
			outText.textContent = outAsText;
			outText.select();
			document.execCommand('copy');
			break;
	/* Can't get this to work yet	
		case "paste":
			outText.textContent = "test";
			outText.select();
			document.execCommand('paste');
			
			var matchSVG = outText.textContent.match(/^<svg.*?<\/svg>/);
			console.log(outText.textContent);
			if (matchSVG) {
				console.log(outText.textContent);
				// Not just if(outText.textContent.match(/^<svg.*?<\/svg>/)) to avoid injection attacks where you end an SVG, run a script, then start another
				if (outText.textContent.match(/^<svg.*?<\/svg>/)[0].length === outText.textContent.length) {
					svgCont.innerHTML = svgCont.innerHTML.replace(/^.*?<\/svg>/, outText.textContent).replace(/<svg /, "<svg id=\"mainsvg\" ");
					return 0;
				}
			}
			return "Pasted content is not a valid SVG";
			break; */ 
		case "save":
			if (arguments.length === 1) {
				download('new.svg', outAsText);
			} else {
				download(arguments[1]+'.svg', outAsText);
			}
			break;
		default:
			return "Unknown argument";
			break;
		}
		return 0;
	}
	
	return "'io' takes at least 1 argument";
}





var inlineFunctions = [];

NUM_OF_CONSOLE_LINES = 16;
