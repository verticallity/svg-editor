var svgCont = document.getElementById('svgcont');
var outText = document.getElementById('outtext');

// Need to seperate the word '<s vg>' (even in comments) because Chrome is idiotic and interprets that as meaning that the whole document is an SVG and won't execute it...
// What even is the state of the internet at this point.
svgCont.innerHTML = "<s" + "vg id=\"mainsvg\" width=\"300\" height=\"300\"></svg><s" + "vg id=\"preview\" width=\"300\" height=\"300\"></svg><s" + "vg id=\"bgplane\" width=\"300\" height=\"300\"></svg>";

var preview = document.getElementById('preview');
var mainSVG = document.getElementById('mainsvg');
var bgPlane = document.getElementById('bgplane');
bgPlane.setAttribute("style", "background-color: #fafafa");

var svgInfo =
{
	width  : 300,
	height : 300
};

var brushInfo =
{
	width  : 1,
	cap    : "none",
	join   : "none",
	stroke : "black",
	fill   : "none"
}
